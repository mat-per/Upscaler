# app_profile.py: these variables are set from within bin/@appname@.py.in
#
# Copyright (C) 2022 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

APP_ID = 'io.gitlab.theevilskeleton.Upscaler'
PROFILE = 'default'
APP_NAME = 'Upscaler'
APP_VERSION = '1.1.2'
APP_ICON_NAME = APP_ID
