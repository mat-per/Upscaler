using Gtk 4.0;
using Adw 1;

template UpscalerWindow : Adw.ApplicationWindow {
  default-width: 700;
  default-height: 600;
  width-request: 300;
  height-request: 300;
  title: _("Upscaler");

  Box {
    orientation: vertical;

    HeaderBar {
      [end]
      MenuButton {
        icon-name: "open-menu-symbolic";
        menu-model: main_menu;
        tooltip-text: _("Main Menu");
      }

    }

    Adw.ToastOverlay toast {
      Stack stack_upscaler {
        transition-type: crossfade;

        StackPage {
          name: "stack_welcome_page";
          child:
          Adw.StatusPage {
            icon-name: "io.gitlab.theevilskeleton.Upscaler-symbolic";
            title: _("Upscaler");
            description: _("Drag and drop images here");
            hexpand: true;
            vexpand: true;
            child:
            Box {
              orientation: vertical;
              spacing: 12;

              Button button_input {
                valign: center;
                halign: center;
                label: _("_Open File…");
                use-underline: true;

                styles [
                  "suggested-action",
                  "pill",
                ]
              }
            }

            ;
          }

          ;
        }

        StackPage {
          name: "stack_loading";
          child:
          Adw.StatusPage {
            Spinner spinner_loading {
              valign: center;
            }
          }

          ;
        }

        StackPage {
          name: "stack_drop";
          child:
          Adw.StatusPage drop_overlay {
            icon-name: "system-run-symbolic";
            title: _("Drop Here to Open");

            styles [
              "dragndrop_overlay"
            ]
          }

          ;
        }

        StackPage {
          name: "stack_upscale";
          child:
          Adw.PreferencesPage {
            Adw.PreferencesGroup {
              Picture image {}
                height-request: 192;
            }

            Adw.PreferencesGroup {
              title: _("Properties");

              Adw.ActionRow action_image_size {
                title:  _("Image Size");

                styles [
                  "property",
                ]
              }

              Adw.ActionRow action_upscale_image_size {
                title:  _("Image Size After Upscaling");

                styles [
                  "property",
                ]
              }
            }

            Adw.PreferencesGroup {
              title: _("Options");

              // Scaling is broken in Real-ESRGAN with values other than 4.

              // Adw.ActionRow {
              //   title: _("Upscale Ratio");
              //   subtitle: _("The amount of times the resolution of the image will increase.");

              //   SpinButton {
              //     numeric: true;
              //     valign: center;
              //     adjustment:
              //     Adjustment spin_scale {
              //       step-increment: 1;
              //       value: 4;
              //       lower: 2;
              //       upper: 4;
              //     }

              //     ;
              //   }
              // }

              Adw.ComboRow combo_models {
                title: _("Type of Image");
                model:
                StringList string_models {}

                ;
              }

              Adw.ActionRow {
                title: _("Save Location");
                activatable-widget: button_output;

                Button button_output {
                  valign: center;

                  Box {
                    spacing: 6;

                    Image {
                      icon-name: "document-open-symbolic";
                    }

                    Label label_output {
                      label: _("(None)");
                    }
                  }
                }
              }
            }

            Adw.PreferencesGroup {
              Button button_upscale {
                valign: center;
                halign: center;
                label: _("_Upscale");
                tooltip-text: _("Save location is missing.");
                use-underline: true;
                sensitive: false;

                styles [
                  "suggested-action",
                  "pill",
                ]
              }
            }
          }

          ;
        }


        StackPage {
          name: "stack_upscaling";
          child:
          Adw.StatusPage {
            title: _("Upscaling…");
            description: _("This could take a while.");

            Box {
              orientation: vertical;
              spacing: 36;

              ProgressBar progressbar {
                valign: center;
                halign: center;
                show-text: true;
                text: _("Loading…");
              }

              Button button_cancel {
                valign: center;
                halign: center;
                label: _("_Cancel");
                use-underline: true;

                styles [
                  "destructive-action",
                  "pill",
                ]
              }
            }
          }

          ;
        }
      }
    }
  }
}

menu main_menu {
  section {
    item {
      label: _("New Window");
      action: "app.new-window";
    }

    item {
      label: _("Open…");
      action: "app.open";
    }
  }

  section {
    // item {
    //   label: _("Preferences");
    //   action: "app.preferences";
    // }

    item {
      label: _("Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }

    item {
      label: _("About Upscaler");
      action: "app.about";
    }
  }
}
