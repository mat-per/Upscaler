# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the upscaler package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: upscaler\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-04 16:28+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/io.gitlab.theevilskeleton.Upscaler.desktop.in:3
msgid "@application_name@"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.desktop.in:4
msgid "Upscale"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.desktop.in:5
#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:7
msgid "Upscale and enhance images"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.desktop.in:11
msgid "image;upscale;upscaling;processing"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:5
#: upscaler/gtk/window.blp:9 upscaler/gtk/window.blp:33
msgid "Upscaler"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:6
msgid "Upscaler Contributors"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:13
msgid ""
"Upscaler allows you to upscale and enhance a given image. It is a front-end "
"for Real-ESRGAN ncnn Vulkan."
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:26
msgid "image"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:27
msgid "upscale"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:28
msgid "upscaling"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:29
msgid "processing"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:30
msgid "enhance"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:31
msgid "enhancing"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:32
msgid "artificial intelligence"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:33
msgid "ai"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:50
msgid "Overview"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:71
msgid "Fix version"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:78
msgid "Update appstream file"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:85
msgid "Replace Upscaling dialog with page"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:86
msgid "Add Open With functionality"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:87
msgid "Improve appstream"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:88
msgid "Check algorithm output in case of failure"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:89
msgid "Add percentage"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:90
msgid "Improved icon"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:91
msgid "Suggest file name when selecting output location"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:92
msgid "Rename \"Open File\" to \"Open Image\" for consistency"
msgstr ""

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:98
msgid "Initial Upscaler release"
msgstr ""

#: upscaler/file_chooser.py:40
msgid "Select an image"
msgstr ""

#: upscaler/file_chooser.py:66
msgid "No file extension was specified"
msgstr ""

#: upscaler/file_chooser.py:71
#, python-brace-format
msgid "’{filename}’ is an unsupported format"
msgstr ""

#: upscaler/file_chooser.py:79
msgid "Select output location"
msgstr ""

#: upscaler/filters.py:41
msgid "Supported image files"
msgstr ""

#: upscaler/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: upscaler/gtk/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr ""

#: upscaler/gtk/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Quit"
msgstr ""

#: upscaler/gtk/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Open Image"
msgstr ""

#: upscaler/gtk/window.blp:34
msgid "Click “Open Image…” or drag an image here."
msgstr ""

#: upscaler/gtk/window.blp:45
msgid "_Open Image…"
msgstr ""

#: upscaler/gtk/window.blp:78
msgid "Drop Here to Open"
msgstr ""

#: upscaler/gtk/window.blp:98
msgid "Properties"
msgstr ""

#: upscaler/gtk/window.blp:101
msgid "Image Size"
msgstr ""

#: upscaler/gtk/window.blp:109
msgid "Post-upscale Image Size"
msgstr ""

#: upscaler/gtk/window.blp:110
msgid "Image size after it is upscaled."
msgstr ""

#: upscaler/gtk/window.blp:119
msgid "Options"
msgstr ""

#: upscaler/gtk/window.blp:143
msgid "Type of Image"
msgstr ""

#: upscaler/gtk/window.blp:151
msgid "Save Location"
msgstr ""

#: upscaler/gtk/window.blp:165 upscaler/window.py:122
msgid "(None)"
msgstr ""

#: upscaler/gtk/window.blp:176
msgid "_Upscale"
msgstr ""

#: upscaler/gtk/window.blp:177
msgid "Save location is missing."
msgstr ""

#: upscaler/gtk/window.blp:197
msgid "Upscaling…"
msgstr ""

#: upscaler/gtk/window.blp:198
msgid "This could take a while."
msgstr ""

#: upscaler/gtk/window.blp:208 upscaler/window.py:186
msgid "Loading…"
msgstr ""

#: upscaler/gtk/window.blp:214 upscaler/window.py:345
msgid "_Cancel"
msgstr ""

#: upscaler/gtk/window.blp:233
msgid "Could not Load Image"
msgstr ""

#: upscaler/gtk/window.blp:234
msgid "This image could be corrupted or may use an unsupported file format."
msgstr ""

#: upscaler/gtk/window.blp:254
msgid "New Window"
msgstr ""

#: upscaler/gtk/window.blp:259
msgid "Open Image…"
msgstr ""

#: upscaler/gtk/window.blp:271
msgid "Keyboard Shortcuts"
msgstr ""

#: upscaler/gtk/window.blp:276
msgid "About Upscaler"
msgstr ""

#: upscaler/main.py:98
msgid "Algorithms by"
msgstr ""

#: upscaler/main.py:105
msgid "Code and Design Borrowed from"
msgstr ""

#: upscaler/main.py:115
msgid "Sample Image from"
msgstr ""

#: upscaler/main.py:168
msgid ""
"Jürgen Benvenuti <gastornis@posteo.org>\n"
"Philip Goto <philip.goto@gmail.com>\n"
"Sabri Ünal <libreajans@gmail.com>\n"
"yukidream https://fosstodon.org/@yukidream\n"
"Anatoly Bogomolov <tolya.bogomolov2019@gmail.com>\n"
"Åke Engelbrektson <eson@svenskasprakfiler.se>\n"
"Onuralp SEZER <thunderbirdtr@fedoraproject.org>"
msgstr ""

#: upscaler/window.py:85
msgid "Photo"
msgstr ""

#: upscaler/window.py:86
msgid "Cartoon/Anime"
msgstr ""

#: upscaler/window.py:227
msgid "Upscaling Cancelled"
msgstr ""

#: upscaler/window.py:245
msgid "Upscaled {basename(self.output_file_path)}"
msgstr ""

#: upscaler/window.py:250
msgid "Image upscaled"
msgstr ""

#: upscaler/window.py:251 upscaler/window.py:259 upscaler/window.py:263
#: upscaler/window.py:271
msgid "Open"
msgstr ""

#: upscaler/window.py:256
msgid "Upscaling Completed"
msgstr ""

#: upscaler/window.py:262
msgid "Image upscaled with warnings"
msgstr ""

#: upscaler/window.py:268
msgid "Upscaling Completed with Warnings"
msgstr ""

#: upscaler/window.py:275 upscaler/window.py:312
msgid "Error while processing"
msgstr ""

#: upscaler/window.py:301
msgid "Error copied to clipboard"
msgstr ""

#: upscaler/window.py:305
msgid "_Copy to clipboard"
msgstr ""

#: upscaler/window.py:307
msgid "_Dismiss"
msgstr ""

#: upscaler/window.py:311
msgid "Upscaling Failed"
msgstr ""

#: upscaler/window.py:319
msgid "Model name: {}"
msgstr ""

#: upscaler/window.py:338
msgid "Stop upscaling?"
msgstr ""

#: upscaler/window.py:339
msgid "You will lose all progress."
msgstr ""

#: upscaler/window.py:346
msgid "_Stop"
msgstr ""

#: upscaler/window.py:411
msgid "Incompatible or Missing Vulkan Drivers"
msgstr ""

#: upscaler/window.py:412
msgid ""
"The Vulkan drivers are either not installed or incompatible with the "
"hardware. Please make sure that the correct Vulkan drivers are installed for "
"the appropriate hardware."
msgstr ""

#: upscaler/window.py:413
msgid "_Exit Upscaler"
msgstr ""
